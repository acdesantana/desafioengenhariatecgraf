package desafio.engenharia.tecgraf.model;

import java.io.Serializable;

import desafio.engenharia.tecgraf.util.Messages;
import edu.princeton.cs.introcs.StdOut;

public class Matricula implements Serializable{

	private static final long serialVersionUID = 6694974210994042820L;
	private final String HEXADECIMAL = "0123456789ABCDEF";
	
	private String matr;
	private String dv;
	
	
	public String calculaDV(String matr) throws Exception{
		try {
			char[] arr = matr.toCharArray();
			int sum = 0;
			
			for(int i=0, j=5, prod=1; i < arr.length; i++, j--) {
				prod = Character.getNumericValue(arr[i]) * j;
				sum += prod;
			}
			
			String hex = HEXADECIMAL; 
			int mod = sum % 16; 
			char dv = hex.charAt(mod);
			
			return Character.toString(dv);
		}catch(Exception e) {
			e.printStackTrace();
			StdOut.println(Messages.PROBLEMA_INESPERADO_CALC_DV);
			throw e;
		}
	}
	
	public String verificaDV(String matr) throws Exception{
		String matrDv = matr.split("-")[1];
		String dv = this.calculaDV(matr.split("-")[0]);
		String dvConfere = (matrDv.equals(dv)) ? Messages.VERDADEIRO : Messages.FALSO; 
		return dvConfere.toString();
	}
	
	
	public String getMatr() {
		return matr;
	}
	public void setMatr(String Matr) {
		this.matr = Matr;
	}
	
	public String getDv() {
		return dv;
	}
	public void setDv(String dv) {
		this.dv = dv;
	}
	
}
