package desafio.engenharia.tecgraf.main;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import desafio.engenharia.tecgraf.model.Matricula;
import desafio.engenharia.tecgraf.util.Messages;
import desafio.engenharia.tecgraf.util.ReadWriteFile;
import edu.princeton.cs.introcs.StdIn;
import edu.princeton.cs.introcs.StdOut;

public class Main implements Serializable{
	
	private static final long serialVersionUID = -8134442149214005596L;
	private static Matricula matricula = new Matricula();
	private static ReadWriteFile rd = new ReadWriteFile();

	
	public static void main( String[] args ) {
		
		try {
			StdOut.println();StdOut.println(Messages.INICIADO);

			StdOut.println();StdOut.println(Messages.INF_PATH_FILES);
			String pathFiles = StdIn.readLine();
			
			StdOut.println();StdOut.print(Messages.LEITURA_MATRIC_SEM_DV);
			List<String> matrDVs = processaArquivo(pathFiles, Messages.FILE_MATRIC_SEM_DV, Messages.FILE_MATRIC_COM_DV, Messages.CALCULA_DV, true);
			
			StdOut.println();StdOut.print(Messages.LEITURA_VERIFICA_MATRS);
			List<String> matrVerif = processaArquivo(pathFiles, Messages.FILE_MATRIC_VERIFICAR_DV, Messages.FILE_MATRIC_VERIFICADAS_DV, Messages.VERIFICA_MATRS, false);
			
			StdOut.println();StdOut.println(Messages.TOTAL + matrDVs.size() + Messages.DV_PROCESSADO);
			StdOut.println(Messages.TOTAL + matrVerif.size() + Messages.VERIF_PROCESSADAS);
			StdOut.println();StdOut.println(Messages.FINALIZADO);
				
		}catch(Exception e) {
			e.printStackTrace();
			StdOut.println(Messages.PROBLEMA_INESPERADO);
		}
    }
	
	
	private static List<String> processaArquivo(String path, String input, String output, String message, boolean calculaDV) throws Exception{
		String asbPath = path.concat("//").concat(input);
		List<String> matriculas = rd.readFile(asbPath);
		StdOut.println(Messages.FEITO);
		
		StdOut.println(message);
		List<String> matrProcs = new ArrayList<String>();
		if(calculaDV) {
			for(int m=0; m < matriculas.size(); m++) {
				String matr = matriculas.get(m);
				String dv = matricula.calculaDV(matr);
				matrProcs.add(matr.concat("-").concat(dv));
			}
		}else {
			for(int m=0; m < matriculas.size(); m++) {
				String matr = matriculas.get(m);
				String digitoConferido = matricula.verificaDV(matr);
				matrProcs.add(matr.concat(digitoConferido));
			}
		}
		StdOut.println(Messages.GRAVA_ARQUIVO);
		rd.writeFile(output, matrProcs);
		
		return matrProcs;
	}
}