package desafio.engenharia.tecgraf.util;

public class Messages {

	public static final String STR_EMPTY = "";
	public static final String VERDADEIRO = "  verdadeiro"; 
	public static final String FALSO = "  falso";
	public static final String FEITO = " Feito.";
	public static final String TOTAL = "Total de ";
	
	public static final String FILE_MATRIC_SEM_DV = "matriculasSemDV.txt";
	public static final String FILE_MATRIC_COM_DV = "matriculasComDV.txt";
	public static final String FILE_MATRIC_VERIFICAR_DV = "matriculasParaVerificar.txt";
	public static final String FILE_MATRIC_VERIFICADAS_DV = "matriculasVerificadas.txt";
	
	public static final String INF_PATH_FILES = "Informe o caminho único para as matrículas sem DV e matrículas com DV a verificar: ";
	
	public static final String LEITURA_MATRIC_SEM_DV = "Lendo arquivo com matrículas sem dv...";
	public static final String CALCULA_DV = "Calculando dígitos verificadores...";
	
	public static final String LEITURA_VERIFICA_MATRS = "Lendo arquivo com matrículas a verificar...";
	public static final String VERIFICA_MATRS = "Verificando matrículas com dv...";
	public static final String PATH_ARQUIVO_SAIDA = "Arquivo salvo em ";
	public static final String IMPOSSIVEL_ESCREVER_ARQUIVO = "Não é possível escrever no arquivo.";
	
	public static final String GRAVA_ARQUIVO = "Gravando arquivo...";
	public static final String ERRO_LEITURA_ARQUIVO = "Erro na leitura do arquivo: ";
	public static final String NAO_ENCONTROU_ARQUIVO = "Não foi possível encontrar o arquivo: ";
	public static final String PROBLEMA_INESPERADO = "Ocorreu um problema inesperado.";
	public static final String PROBLEMA_INESPERADO_CALC_DV = "Ocorreu um problema inesperado ao calcular DV.";
	
	public static final String INICIADO = "Desafio Engenharia Tecgraf iniciado.";
	public static final String FINALIZADO = "Processamento finalizado com sucesso.";
	public static final String DV_PROCESSADO = " matrículas com DV calculado.";
	public static final String VERIF_PROCESSADAS = " matrículas com DV verificado.";

	
}
