package desafio.engenharia.tecgraf.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import edu.princeton.cs.introcs.StdOut;

public class ReadWriteFile implements Serializable{

	private static final long serialVersionUID = 1600066346011437825L;

	
	public List<String> readFile(String filename) throws Exception{
		BufferedReader bf = null;
		List<String> matriculas = null;
		try {

			try {
				File f = new File(filename);
				String absPath = f.getAbsolutePath();
				bf = new BufferedReader(new FileReader(absPath));
				
				if(bf.ready()) {
					matriculas = bf.lines().collect(Collectors.toList());
				}
				bf.close();
				
			}catch(FileNotFoundException e) {
				e.printStackTrace();
				StdOut.println(Messages.NAO_ENCONTROU_ARQUIVO);
				throw e;
			
			}catch(Exception g) {
				g.printStackTrace();
				StdOut.println(Messages.ERRO_LEITURA_ARQUIVO);
				throw g;
			}
		
		}catch(Exception f) {
			f.printStackTrace();
			StdOut.println(Messages.PROBLEMA_INESPERADO);
			throw f;
		}
		return matriculas;
	}
	
	
	public void writeFile(String output, List<String> matriculas) {
		try {
			File file = new File(output);
			String absPath = file.getAbsolutePath();
			PrintWriter print = new PrintWriter(new BufferedWriter(new FileWriter(absPath)));
			
			for(String matr : matriculas) {
				print.println(matr);
			}
			print.close();
			
			StdOut.println(Messages.PATH_ARQUIVO_SAIDA + absPath);
			
		} catch (IOException e) {
			e.printStackTrace();
			StdOut.println(Messages.IMPOSSIVEL_ESCREVER_ARQUIVO);
			
		} catch (Exception e) {
			e.printStackTrace();
			StdOut.println(Messages.PROBLEMA_INESPERADO);
		}
	}
	
	public void deleteFile(String filename) {
		try {
			File f = new File(filename);
			if(f.exists()) {
				f.delete();
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			StdOut.println(Messages.PROBLEMA_INESPERADO);
		}
	}
	
}
