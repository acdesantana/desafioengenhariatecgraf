package desafio.engenharia.tecgraf.junit;

import org.junit.Assert;
import org.junit.Test;

import desafio.engenharia.tecgraf.model.Matricula;
import desafio.engenharia.tecgraf.util.Messages;


public class MatriculaTest{
	
	private String matriculaOk = "9876-E";
	private String matriculaSemDV = "9876";
	private String matriculaErro = "9876-D";
	private Matricula matricula = new Matricula();
	
	
	@Test
	public void calculaDvTest() {
		try {
			String dv = matricula.calculaDV(matriculaSemDV);
			Assert.assertEquals("E", dv);
			
		}catch(Exception e) {
			Assert.fail();
		}
	}
	
	@Test
	public void calculaDvErrorTest() {
		try {
			String dv = matricula.calculaDV(matriculaSemDV.substring(0,2));
			Assert.assertEquals("D", dv);
			
		}catch(Exception e) {
			Assert.fail();
		}
	}
	
	@Test
	public void verificaDvTest() {
		try {
			String dvConfere = matricula.verificaDV(matriculaOk);
			Assert.assertTrue(Messages.VERDADEIRO.equals(dvConfere));
			
		}catch(Exception e) {
			Assert.fail();
		}
	}
	
	@Test
	public void verificaDvErrorTest() {
		try {
			String dvConfere = matricula.verificaDV(matriculaErro);
			Assert.assertTrue(Messages.FALSO.equals(dvConfere));
			
		}catch(Exception e) {
			Assert.fail();
		}
	}
	
	
}
