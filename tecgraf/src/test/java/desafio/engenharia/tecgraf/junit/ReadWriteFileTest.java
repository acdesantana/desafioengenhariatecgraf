package desafio.engenharia.tecgraf.junit;

import java.io.FileNotFoundException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import desafio.engenharia.tecgraf.util.Messages;
import desafio.engenharia.tecgraf.util.ReadWriteFile;

public class ReadWriteFileTest {

	private ReadWriteFile rd = new ReadWriteFile();
	
	
	@Test
	public void readFileTest() {
		try {
			List<String> matrLidas = rd.readFile(Messages.FILE_MATRIC_SEM_DV);
			Assert.assertTrue(matrLidas.contains("3429"));
			Assert.assertTrue(matrLidas.contains("7038"));
			
		}catch(Exception e) {
			Assert.fail();
		}
	}
	
	@Test
	public void fileNotFoundTest() {
		try {
			rd.readFile("arquivoNaoExiste.txt");
		}catch(Exception e) {
			Assert.assertEquals(FileNotFoundException.class, e.getClass());
		}
	}
	
	@Test
	public void writeFileTest() {
		try {
			List<String> matrs = rd.readFile("arquivoLidoTeste.txt");
			rd.writeFile("arquivoEscritoTeste.txt", matrs);
			
			List<String> wrote = rd.readFile("arquivoEscritoTeste.txt");
			Assert.assertTrue(wrote.contains("3429"));
			Assert.assertTrue(wrote.contains("7038"));
			
			rd.deleteFile("arquivoEscritoTeste.txt");
		
		}catch(Exception e) {
			Assert.fail();
		}
	}
	
	@Test
	public void writeFileExceptionTest() {
		try {
			List<String> matrs = rd.readFile("arquivoLidoTeste.txt");
			rd.writeFile("/pasta_nao_existe/arquivoEscritoTeste.txt", matrs);
			
		}catch(Exception e) {
			Assert.assertEquals(FileNotFoundException.class, e.getClass());
		}
	}
	
	@Test
	public void deleteFileTest() {
		try {
			List<String> matrs = rd.readFile("arquivoLidoTeste.txt");
			rd.writeFile("arquivoEscritoTeste.txt", matrs);
			rd.deleteFile("arquivoEscritoTeste.txt");
			rd.readFile("arquivoEscritoTeste.txt");
			
		}catch(Exception e) {
			Assert.assertEquals(FileNotFoundException.class, e.getClass());
		}
	}
}
